$(document).ready(function() {

  $(".book-carousel").owlCarousel({
  	items: 4,
    itemsDesktop : [1199,3],
    itemsTablet: [992, 2],
    itemsMobile: [768, 1],
    navigation: true,
    navigationText: [
      "<i class='glyphicon glyphicon-chevron-left'></i>",
      "<i class='glyphicon glyphicon-chevron-right'></i>"
      ],
 
  });

  $('.masonry').masonry({
    itemSelector: '.masonry__item'
  });
 
});

google.maps.event.addDomListener(window, 'load', initMap);

function initMap() {
  var mapDiv = document.getElementById('map');

  var location = {lat: 50.827475, lng: -0.170321};

  var map = new google.maps.Map(mapDiv, {
      center: {lat: 50.927475, lng: -0.170321}, 
      zoom: 13,
      scrollwheel: false,
  });

  var marker = new google.maps.Marker({
      position: location,
      map: map,
      title: 'Joosr HQ'
  });

  var infowindow = new google.maps.InfoWindow({
    content: "<h5>Joosr HQ</h5><p>34 3rd Ave, Brighton and Hove, Hove BN3 2PD</p><a href='https://maps.google.com/?q=34 3rd Ave, Brighton and Hove, Hove BN3 2PD' target='_blank'>View on Google Map</a>"
  });

  infowindow.open(map, marker);

  marker.addListener('click', function() {
    infowindow.open(map, marker);
  });
}